run:
	poetry run python -m pylibvirt -t pylibvirt/template/template.yml

install:
	poetry install

test:
	pyenv install -s 3.7.11
	pyenv install -s 3.8.11
	pyenv install -s 3.9.6
	pyenv local 3.7.11 3.8.11 3.9.6
	tox

lint:
	poetry run flake8 ./pylibvirt/

clean:
	rm -Rf .pytest_cache .tox htmlcov pylibvirt.egg-info .coverage dist

build: install
	poetry build
	poetry run twine check --strict dist/*

upload: install
	poetry run twine upload -s dist/*

.PHONY: clean
