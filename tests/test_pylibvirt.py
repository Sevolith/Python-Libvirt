#!/usr/bin/env python

"""Tests for `pylibvirt_` package."""

from pylibvirt.modules import Device, Volume, DirStorage, Domain, Network
from pylibvirt.modules.devices import GraphicDevice, SoundDevice, VideoDevice, \
    UsbDevice, FileSystemDevice, ControllerDevice, RngDevice, NetworkInterfaceDevice, \
    XmlGenerator, CpuDevice


def test_xml_generator():
    data = {"ip": {
        "attr": {
            "address": '127.0.0.1',
            "netmask": '255.0.0.0'
        },
        "children": {
            "test": {
                "text": "Hello"
            }
        }
    }}
    xml = XmlGenerator()
    result = xml.generate_xml_element(data=data)
    assert result == '<ip address="127.0.0.1" ' \
                     'netmask="255.0.0.0"><test>Hello</test></ip>'


def test_graphic_device():
    GraphicDevice()
    module = GraphicDevice()
    result = module.generate_xml()
    assert result == '<graphics type="spice" autoport="yes"><listen type="address" ' \
                     '/><image compression="off" /><gl enable="no" /></graphics>'


def test_sound_device():
    sound = SoundDevice()
    sound.model = 'abc'
    sound.generate_data()
    result = sound.generate_xml()
    assert result == '<sound model="abc" />'


def test_fs_device():
    module = FileSystemDevice(source="/", target="/")
    result = module.generate_xml()
    assert result == '<filesystem type="mount" accessmode="mapped"><source dir="/" ' \
                     '/><target dir="/" /></filesystem>'


def test_generate_devices():
    CpuDevice()
    VideoDevice()
    UsbDevice(vendor_id='abc', product_id='bca')
    FileSystemDevice(source='abc', target='bca')
    ControllerDevice()
    NetworkInterfaceDevice()


def test_generate_module():
    Device(name='abc')
    Volume(name='abc', capacity=2)
    DirStorage(pool_name='abc', path='/')
    Network(name='abc', ip4_cidr='10.0.0.0/24', dhcp_start='10.0.0.1',
            dhcp_stop='10.0.0.5')


def test_domain():
    rng = RngDevice()
    dom = Domain(name='abc', devices=[rng], feature=['acpi', 'apci', {
        'kvm': {'hidden': {'state': 'on'}, 'hint-dedicated': {'state': 'on'},
                'poll-control': {'state': 'on'}}}])
    dom.memory = dom.set_memory(max_memory=12)
